const navIconEl = document.querySelector('.nav__icon');
const navCloseEl = document.querySelector('.nav__close')
const navListEl = document.querySelector('.nav__list');
const navBgOverlayEl = document.querySelector('.nav__bgOverlay');

const open = () => {
    navListEl.classList.add('show');
    navBgOverlayEl.classList.add('active')
    document.body.style = 'visibility:visible;height:100vh;width:100vw;overflow:hidden;'
}
const close = () => {
    navListEl.classList.remove('show')
    navBgOverlayEl.classList.remove('active')
    document.body.style = 'visibility:visible;height:initial;width:100%;'
}
navIconEl.addEventListener('click',open)
navCloseEl.addEventListener('click',close)
navBgOverlayEl.addEventListener('click',close)